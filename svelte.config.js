import preprocess from 'svelte-preprocess';
import adapter from '@sveltejs/adapter-node';

/** @type {import('@sveltejs/kit').Config} */
const config = {
    preprocess: preprocess(),

    kit: {
        // SET THE PATHS HERE
        paths: { assets: "", base: "" },
        adapter: adapter()
    }
};

export default config;