# Stage 1: Build stage
FROM node:20.10.0 as build
WORKDIR /usr/src/app
# Copy only necessary files for npm install
COPY package.json ./
# Build the project
RUN npm install
COPY . .
RUN npm run build
# Copy the entire project
EXPOSE 3000

CMD ["node", "./build/index.js"]
