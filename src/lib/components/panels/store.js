import { writable } from 'svelte/store';

export const numberOfEmails = writable(0);
export const selectMode = writable("");
