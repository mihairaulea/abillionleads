
type Props = {
    queryObject: any
    page: number
}

async function fetchLeads({
    queryObject,
    page
}: Props) {
    try {
        const dataUrl =
            import.meta.env.VITE_SERVER_URL +
            "/search/?value=" +
            encodeURIComponent(JSON.stringify(queryObject)) +
            "&page=" +
            page;

        const response = await fetch(dataUrl)
        const dataReturn: any = await response.json()

        return {
            count: parseInt(dataReturn.count),
            results: dataReturn.data
        }
    } catch (error: any) {
        console.log(error);
        return {
            count: undefined,
            results: []
        };
    };
}

export default fetchLeads