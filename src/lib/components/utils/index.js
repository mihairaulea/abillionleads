export function debounce(func, timeout = 300) {
  let timer;
  return (...args) => {
    clearTimeout(timer);
    timer = setTimeout(() => {
      func.apply(this, args);
    }, timeout);
  };
}

export function onoutsideclick(node, callback) {
  function onClick(e) {
    const { top, right, bottom, left } = node.getBoundingClientRect();
    if (!(left < e.x && e.x < right && top < e.y && e.y < bottom)) callback(e);
  }
  window.addEventListener("click", onClick);
  return {
    destroy() {
      window.removeEventListener("click", onClick);
    },
  };
}


export function formatNumber(num) {
  if (num >= 100000000) {
    return (num / 1000000).toFixed(0) + "M";
  } else if (num >= 10000000) {
    return (num / 1000000).toFixed(1) + "M";
  } else if (num >= 1000000) {
    return (num / 1000000).toFixed(2) + "M";
  } else if (num >= 10000) {
    return (num / 1000).toFixed(0) + "k";
  } else if (num >= 1000) {
    return (num / 1000).toFixed(1) + "k";
  } else {
    return num.toLocaleString();
  }
}