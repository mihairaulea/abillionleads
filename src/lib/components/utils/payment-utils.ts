import { loadStripe } from '@stripe/stripe-js'
import axios from 'axios'

const stripePromise = loadStripe(import.meta.env.VITE_STRIPE_PUBLISHABLE_KEY)

export const handleBuyCredits = async ({email,credits}:{email: string,credits: number}) => {
    // Get Stripe.js instance
    const stripe = await stripePromise

    // Call your backend to create the Checkout Session
    const response = await axios.post(
        import.meta.env.VITE_SERVER_URL+ '/stripe/checkout',
        {
            email,
            credits,
        },
    )

    const session = response.data
    console.log(
        '🚀 ~ file: CheckoutForm.jsx:25 ~ handleBuyCredits ~ session:',
        session,
    )

    // When the customer clicks on the button, redirect them to Checkout.
    const result = await stripe?.redirectToCheckout({
        sessionId: session.sessionId,
    })

    if (result?.error) {
        console.log(result.error.message)
    }
}