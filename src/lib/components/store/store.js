import { writable } from "svelte/store";
import { formatNumber } from "../utils";

export const filterCountries = writable({ included: [], excluded: [] });
export const filterCities = writable({ included: [], excluded: [] });
export const filterIndustry = writable({ included: [], excluded: [] });
// new
export const filterSkills = writable({ included: [], excluded: [] });
export const filterInterests = writable({ included: [], excluded: [] });
export const filterCertifications = writable({ included: [], excluded: [] });
export const filterSchoolnames = writable({ included: [], excluded: [] });
export const filterDegrees = writable({ included: [], excluded: [] });
export const filterLanguages = writable({ included: [], excluded: [] });
// end new
export const filterJobTitle = writable({ included: [], excluded: [] });

export const filterSeniority = writable({ included: [], excluded: [] });
export const hasEmail = writable(0);
export const hasPhoneNumber = writable(0);

export const filterBusinessName = writable({ included: [], excluded: [] });
// i think i should rename these
export const filterCompanySizes = writable({ included: [], excluded: [] });
export const filterBusinessIndustry = writable({ included: [], excluded: [] });
// end new
export const checkedItems = writable([]);

export const numberOfResults = writable(0);
export const numberOfResultsFormated = writable('Loading...');

const unsubscribeCheckedItems = checkedItems.subscribe(checkedItems => {
  console.log('New checked items:', checkedItems);
});

const unsubscribeNumberOfResults = numberOfResults.subscribe(item => {
  console.log('=========================//////////==========================');
  console.log('updated ' + formatNumber(item));
  numberOfResultsFormated.update(() => formatNumber(item));
  //console.log(JSON.stringify(numberOfResultsFormated)+' /////////');
});

const unsubscribeNumberFormatted = numberOfResultsFormated.update(item => {
  console.log(item + ' --==-=-=-=+++++++');
})

export const arrayOfFilters = {
  'location_country': filterCountries,
  'location_locality': filterCities,
  'industry': filterIndustry,
  'skills': filterSkills,
  'interests': filterInterests,
  'certifications': filterCertifications,
  'schoolnames': filterSchoolnames,
  'degrees': filterDegrees,
  'languages': filterLanguages,
  'job_title': filterJobTitle,
  //'hasEmail':hasEmail,
  //'hasPhoneNumber': hasPhoneNumber,
  'job_company_name': filterBusinessName,
  'job_company_size': filterCompanySizes,
  'job_company_industry': filterBusinessIndustry,
  'job_title_levels': filterSeniority
  //'companyName': companyName
};

// filter out job title, only add if in top 10k or 30k