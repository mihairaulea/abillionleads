import { writable } from "svelte/store";

export const showActionPopup = writable("");

function isLargeScreen() {
    if (typeof window !== 'undefined') {
        return window.matchMedia('(min-width: 992px)').matches;
    }
    return false;
}

export const sidebarOpen = writable(isLargeScreen());

