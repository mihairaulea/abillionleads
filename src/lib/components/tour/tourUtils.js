

const key = import.meta.env.VITE_SHOW_TOUR ? Date.now().toString() : "tourShown"
export const isTourShown = () => {
    const tourShownValue = localStorage.getItem(key);

    console.log({ tourShownValue })
    return tourShownValue === "true"
}

export const setTourShown = () => {
    localStorage.setItem(key, "true"); // After the tour is shown, set 'tourShown' to true

}


